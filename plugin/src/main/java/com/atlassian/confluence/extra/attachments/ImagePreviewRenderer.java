package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionBuilder;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.google.common.collect.ImmutableSet;

import java.util.Map;
import java.util.Set;

/**
 * Renders the image previews in the attachments macro.
 */
public class ImagePreviewRenderer {
    private static final String VIEW_FILE_WIDTH = "310";
    private static final String VIEW_FILE_HEIGHT = "250";
    private static final Set<String> VIEW_FILE_MACRO_EXTENSIONS = ImmutableSet.of("ppt", "pptx", "pdf");
    private static final Set<String> IMAGE_EXTENSIONS = ImmutableSet.of("png", "jpg", "jpeg", "gif");

    private final VelocityHelperService velocityHelperService;
    private final XhtmlContent xhtmlContent;

    public ImagePreviewRenderer(VelocityHelperService velocityHelperService, XhtmlContent xhtmlContent) {
        this.velocityHelperService = velocityHelperService;
        this.xhtmlContent = xhtmlContent;
    }

    public boolean willBeRendered(Attachment attachment) {
        final String ext = getFileExtension(attachment);
        return VIEW_FILE_MACRO_EXTENSIONS.contains(ext) || IMAGE_EXTENSIONS.contains(ext);
    }

    /**
     * Renders html for the attachment image preview. Not all types of attachments are supported. For now we preview
     * powerpoint and pdf files via the office connector and image files.
     *
     * @param attachment        The Attachment
     * @param conversionContext The conversion context
     * @return String
     * @throws com.atlassian.confluence.content.render.xhtml.XhtmlException if there is an xhtml exception
     */
    public String render(Attachment attachment, ConversionContext conversionContext) throws XhtmlException {
        String ext = getFileExtension(attachment);

        if (VIEW_FILE_MACRO_EXTENSIONS.contains(ext)) {
            return renderViewFileMacro(attachment, conversionContext);
        } else if (IMAGE_EXTENSIONS.contains(ext)) {
            return renderPreviewImage(attachment);
        } else {
            return "";
        }
    }

    private String getFileExtension(Attachment attachment) {
        int index = attachment.getFileName().lastIndexOf(".");
        return attachment.getFileName().substring(index + 1).toLowerCase();
    }

    /**
     * Renders a simple preview image.
     *
     * @param attachment
     * @return
     */
    private String renderPreviewImage(Attachment attachment) {
        final Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();
        contextMap.put("attachment", attachment);
        return velocityHelperService.getRenderedTemplate("templates/extra/attachments/imagepreview.vm", contextMap);
    }

    /**
     * Renders the view file macro that provides a full fledged slide viewer.
     *
     * @param attachment
     * @param conversionContext
     * @return
     * @throws XhtmlException
     */
    private String renderViewFileMacro(Attachment attachment, ConversionContext conversionContext) throws XhtmlException {
        MacroDefinitionBuilder builder = MacroDefinition.builder();
        builder.withName("viewfile");
        builder.withParameter("name", attachment.getFileName());
        builder.withParameter("width", VIEW_FILE_WIDTH);
        builder.withParameter("height", VIEW_FILE_HEIGHT);

        return xhtmlContent.convertMacroDefinitionToView(builder.build(), conversionContext);
    }
}
