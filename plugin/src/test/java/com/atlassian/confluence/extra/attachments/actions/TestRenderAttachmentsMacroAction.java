package com.atlassian.confluence.extra.attachments.actions;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.stream.XMLStreamException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestRenderAttachmentsMacroAction {
    private RenderAttachmentsMacroAction renderAttachmentsMacroAction;

    @Mock
    private XhtmlContent xhtmlContent;
    @Mock
    private PermissionManager permissionManager;

    @Before
    public void setUp() {
        renderAttachmentsMacroAction = new RenderAttachmentsMacroAction() {
            @Override
            public String getText(String s) {
                return s;
            }
        };

        renderAttachmentsMacroAction.setPermissionManager(permissionManager);
        renderAttachmentsMacroAction.setXhtmlContent(xhtmlContent);
        renderAttachmentsMacroAction.setPage(new Page());
    }

    @After
    public void tearDown() {
        permissionManager = null;
        xhtmlContent = null;
    }

    @Test
    public void testErrorMessageShownIfUserNotPermittedToViewTargetPage() {
        renderAttachmentsMacroAction.validate();
        Assert.assertTrue(renderAttachmentsMacroAction.hasActionErrors());
    }

    @Test
    public void testMacroRenderedWithOldParameter() throws XMLStreamException, XhtmlException {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=true|upload=false}"), any(), any())).thenReturn("");
        renderAttachmentsMacroAction.setOld(true);

        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    @Test
    public void testMacroRenderedWithSortByParameter() throws XMLStreamException, XhtmlException {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=false|upload=false|sortBy=date}"), any(), any())).thenReturn("");

        renderAttachmentsMacroAction.setSortBy("date");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    @Test
    public void testMacroRenderedWithPatternsParameter() throws XMLStreamException, XhtmlException {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=false|upload=false|patterns=^.*\\.txt$}"), any(),
                any())).thenReturn("");

        renderAttachmentsMacroAction.setPatterns("^.*\\.txt$");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    @Test
    public void testMacroWithAllSettableParameters() throws XMLStreamException, XhtmlException {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=true|upload=false|sortBy=date|patterns=^.*\\.txt$}")
                , any(), any())).thenReturn("");

        renderAttachmentsMacroAction.setOld(true);
        renderAttachmentsMacroAction.setSortBy("date");
        renderAttachmentsMacroAction.setPatterns("^.*\\.txt$");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }
}
