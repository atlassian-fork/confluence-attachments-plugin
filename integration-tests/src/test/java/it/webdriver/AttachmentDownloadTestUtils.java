package it.webdriver;

import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.google.common.base.Charsets.UTF_8;
import static java.io.File.createTempFile;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.IOUtils.toByteArray;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

class AttachmentDownloadTestUtils {

    private final ConfluenceRestClient restClient;

    AttachmentDownloadTestUtils(ConfluenceRestClient restClient) {
        this.restClient = restClient;
    }

    void downloadAndVerifyAttachmentsZip(URI downloadUrl, UserWithDetails user, Map<String, byte[]> expectedAttachmentData) throws IOException {
        File downloadedZipFile = createTempFile(AttachmentsMacroWebDriverTest.class.getName(), ".zip");
        downloadedZipFile.deleteOnExit();
        writeByteArrayToFile(downloadedZipFile, fetchUrl(downloadUrl, user, byte[].class));

        for (Map.Entry<String, byte[]> expectedAttachment : expectedAttachmentData.entrySet()) {
            assertZipFileEntryContent(downloadedZipFile, expectedAttachment.getKey(), expectedAttachment.getValue());
        }
    }

    private void assertZipFileEntryContent(File downloadedZipFile, String expectedAttachmentTitle, byte[] expectedAttachmentData) throws IOException {
        final ZipFile zipFile = new ZipFile(downloadedZipFile);

        final ZipEntry zipEntry = zipFile.getEntry(expectedAttachmentTitle);
        assertThat("No entry found in downloaded zip file with filename '" + expectedAttachmentTitle + "'", zipEntry, notNullValue());
        final byte[] downloadedData = toByteArray(zipFile.getInputStream(zipEntry));

        assertTrue(
                String.format(
                        "Download content for attachment '%s' was '%s' but expected '%s'",
                        expectedAttachmentTitle,
                        new String(downloadedData, UTF_8),
                        new String(expectedAttachmentData, UTF_8)
                ),
                Arrays.equals(downloadedData, expectedAttachmentData));

        zipFile.close();
    }

    void fetchUrlAndCheckContent(String expectedContents, URI downloadUrl, UserWithDetails user) {
        assertThat(fetchUrl(downloadUrl, user, String.class), is(expectedContents));
    }

    private <T> T fetchUrl(URI attachmentDownloadUrl, UserWithDetails user, Class<T> type) {
        return restClient.createSession(user).resource().uri(attachmentDownloadUrl).get(type);
    }
}
