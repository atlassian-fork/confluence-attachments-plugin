package it.webdriver.pageobjects;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.binder.Init;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

/**
 * We could use {@link com.atlassian.confluence.webdriver.pageobjects.component.attachment.AttachmentRemovalConfirmationDialog} here,
 * but this version is more robust.
 */
public class AttachmentRemovalConfirmationDialog extends Dialog {
    private final Content attachment;
    private final Content page;

    public AttachmentRemovalConfirmationDialog(Content attachment, Content page) {
        super("attachment-removal-confirm-dialog");
        this.attachment = attachment;
        this.page = page;
    }

    @Init
    @Override
    public void waitUntilVisible() {
        super.waitUntilVisible();
    }

    public AttachmentRemovalConfirmationDialog assertDialogTitleAndContentMatchesAttachment() {
        assertThat(find(".dialog-title").getText(), equalToIgnoringCase("Attachment Deletion Confirmation"));
        assertThat(find(".dialog-panel-body").getText(), containsString(attachment.getTitle()));

        return this;
    }

    public AttachmentsMacroComponent closeDialog() {
        clickCancel();
        waitForRemoval();
        return pageBinder.bind(AttachmentsMacroComponent.class, page);
    }

    public void confirm() {
        clickButton("button-panel-submit-button", false);
        waitForRemoval();
    }
}
