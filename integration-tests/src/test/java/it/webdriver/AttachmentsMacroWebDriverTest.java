package it.webdriver;

import com.atlassian.confluence.api.model.content.AttachmentUpload;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.test.BaseUrlSelector;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.fugue.Pair;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;
import it.webdriver.pageobjects.AttachmentSummary;
import it.webdriver.pageobjects.AttachmentsMacroComponent;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;

import static com.atlassian.confluence.api.model.content.ContentBody.contentBodyBuilder;
import static com.atlassian.confluence.api.model.content.ContentRepresentation.WIKI;
import static com.atlassian.confluence.api.model.content.ContentType.PAGE;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ATTACHMENT_CREATE;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ATTACHMENT_REMOVE;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_EDIT;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.VIEW;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.google.common.base.Charsets.UTF_8;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(ConfluenceStatelessTestRunner.class)
public class AttachmentsMacroWebDriverTest {
    @Rule
    public TestName testName = new TestName();
    @Rule
    public MacroParametersRule macroParametersRule = new MacroParametersRule();

    @Fixture
    private static UserFixture user1 = userFixture().build();
    @Fixture
    private static UserFixture user2 = userFixture().build();

    @Fixture
    private static SpaceFixture spaceFixture = spaceFixture()
            .permission(user1, ATTACHMENT_CREATE, ATTACHMENT_REMOVE, VIEW, PAGE_EDIT)
            .permission(user2, VIEW, ATTACHMENT_CREATE)
            .build();

    private Content page;

    @Inject
    private static ConfluenceRestClient restClient;

    @Inject
    private static BaseUrlSelector baseUrlSelector;

    @Inject
    private static ConfluenceTestedProduct product;

    private static AttachmentDownloadTestUtils testUtils;

    @BeforeClass
    public static void initTestUtils() {
        testUtils = new AttachmentDownloadTestUtils(restClient);
    }

    @Before
    public void setUp() {
        page = contentService(user1.get()).create(Content.builder(PAGE)
                .space(spaceFixture.get())
                .title(testName.getMethodName())
                .body(buildAttachmentMacroDescriptor(pair(macroParametersRule.getMacroParameter())), WIKI)
                .build());
    }

    @Test
    public void emptyAttachmentsTable() {
        loginAndViewAttachmentsMacro();
    }

    @Test
    public void singleAttachmentIsDisplayed() throws IOException {
        final Content attachment = createTextAttachment(user1.get());
        loginAndViewAttachmentsMacro()
                .assertAttachmentRowIsDisplayed(attachment, user1.get())
                .openAttachmentSummary(attachment)
                .assertNoLabels()
                .assertAttachmentVersionIsDisplayed(1, true, user1.get(), "Test file");
    }

    @Test
    public void attachmentLabelsAreDisplayedAndCanBeEdited() throws IOException {
        final Content attachment = createTextAttachment(user1.get());

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);

        attachmentSummary
                .openLabelsDialog()
                .addLabel("my-label")
                .close();

        attachmentSummary.assertDisplayedLabels("my-label");
    }

    @Test
    public void attachmentLabelsHaveCorrectUrl() throws IOException {
        final Content attachment = createTextAttachment(user1.get());

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro().openAttachmentSummary(attachment);

        attachmentSummary.openLabelsDialog()
                .addLabel("my-label")
                .close();

        attachmentSummary.assertDisplayedLabels("my-label");

        String expectedUrl = product.getProductInstance().getBaseUrl() + "/label/" + page.getSpace().getKey() + "/my-label";

        viewAttachmentsMacro().openAttachmentSummary(attachment)
                .assertDisplayedLabelsWithUrl(expectedUrl);
    }

    @Test
    public void attachmentsAreListedByLabel() throws IOException {
        final Content attachment1 = createTextAttachment(user1.get(), "test1.txt");
        final Content attachment2 = createTextAttachment(user1.get(), "test2.txt");
        final Content attachment3 = createTextAttachment(user1.get(), "test3.txt");

        AttachmentsMacroComponent attachmentsMacro = loginAndViewAttachmentsMacro();

        addLabelsToAttachment(attachment1, attachmentsMacro, "a");
        addLabelsToAttachment(attachment2, attachmentsMacro, "a", "b");
        addLabelsToAttachment(attachment3, attachmentsMacro, "a");

        contentService(user1.get()).update(Content.builder(page)
                .version(page.getVersion().nextBuilder().build())
                .body(singletonMap(
                        WIKI, contentBodyBuilder().representation(WIKI).value(
                                buildAttachmentMacroDescriptor(Pair.pair("labels", "a,b"))
                        ).build()
                ))
                .build());
        product.refresh();

        attachmentsMacro.assertAttachmentOrder(attachment2);
    }

    private static void addLabelsToAttachment(Content attachment, AttachmentsMacroComponent attachmentsMacro, final String... labels) {
        AttachmentSummary attachmentSummary = attachmentsMacro.openAttachmentSummary(attachment);
        for (String label : labels) {
            attachmentSummary.openLabelsDialog().addLabel(label).close();
        }
        attachmentsMacro.toggleAttachmentSummary(attachment); // close the summary again to prevent screen overflow from failing the test
    }

    @Test
    public void displayPreviewImage() throws IOException {
        final String filename = "image.png";

        Content attachment = createAttachment(user1.get(), filename, "image/png");
        AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);
        attachmentSummary.hasImagePreview(filename);
    }

    @Test
    public void displayPreviewForPdf() throws IOException {
        final String filename = "slides.pdf";

        Content attachment = createAttachment(user1.get(), filename, "application/pdf");
        AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);
        attachmentSummary.hasPdfPreview(filename);
    }

    @Test
    public void multipleAttachmentSummaries() throws IOException {
        final Content file1 = createTextAttachment(user1.get(), "test1.txt");
        final Content file2 = createTextAttachment(user2.get(), "test2.txt");

        final AttachmentsMacroComponent attachments = loginAndViewAttachmentsMacro();
        attachments.assertAttachmentRowIsDisplayed(file1, user1.get());
        attachments.assertAttachmentRowIsDisplayed(file2, user2.get());

        attachments.toggleAttachmentSummary(file1);

        attachments.getAttachmentSummary(file1).assertIsVisible();
        attachments.getAttachmentSummary(file2).assertIsNotVisible();

        attachments.toggleAttachmentSummary(file2);

        attachments.getAttachmentSummary(file1).assertIsVisible();
        attachments.getAttachmentSummary(file2).assertIsVisible();

        attachments.toggleAttachmentSummary(file1);

        attachments.getAttachmentSummary(file1).assertIsNotVisible();
        attachments.getAttachmentSummary(file2).assertIsVisible();
    }

    @Test
    public void attachmentOrderingByNameAscending() throws IOException {
        final Content alice = createTextAttachment(user1.get(), "alice.txt");
        final Content carol = createTextAttachment(user1.get(), "carol.txt");
        final Content bob = createTextAttachment(user1.get(), "Bob.txt");

        loginAndViewAttachmentsMacro()
                .clickSortByFilename()
                .assertAttachmentOrder(alice, bob, carol);
    }

    @Test
    public void attachmentOrderingByModifiedDateDescending() throws IOException {
        final Content alice = createTextAttachment(user1.get(), "alice.txt");
        final Content carol = createTextAttachment(user1.get(), "carol.txt");
        final Content bob = createTextAttachment(user1.get(), "Bob.txt");

        loginAndViewAttachmentsMacro()
                .clickSortByModifiedDate()  // Clicking this will make the direction reverse, as we have Modified Date (ascending) order by default
                .assertAttachmentOrder(bob, carol, alice);
    }

    @Test
    public void openAttachmentPropertiesEditor() throws IOException {
        final Content attachment = createTextAttachment(user1.get());

        loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment)
                .clickAttachmentProperties()
                .assertTitleMatchesAttachment();
    }

    @Test
    public void deleteAttachment() throws IOException {
        final Content attachment = createTextAttachment(user1.get());

        loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .assertDialogTitleAndContentMatchesAttachment()
                .closeDialog()
                .getAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .confirm();

        viewAttachmentsMacro().assertAttachmentRowIsNotDisplayed(attachment);
    }

    @Test
    public void deleteAttachmentAfterUpload() throws IOException {
        AttachmentsMacroComponent macroComponent = loginAndViewAttachmentsMacro();

        // 'Fake' upload a new attachment version and trigger the refresh
        final Content attachment = createTextAttachment(user1.get());
        macroComponent.refreshAttachments(page.getId(), attachment);

        macroComponent.openAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .confirm();

        viewAttachmentsMacro().assertAttachmentRowIsNotDisplayed(attachment);
    }

    @Test
    public void cannotDeleteAttachmentWithoutPermission() throws IOException {
        final Content attachment = createTextAttachment(user1.get());

        loginAndViewAttachmentsMacro(user2.get())
                .openAttachmentSummary(attachment)
                .assertRemoveAttachmentLinkNotPresent();
    }

    @Test
    public void downloadAllLinkOnlyPresentForTwoOrMoreAttachments() throws IOException {
        assertThat(loginAndViewAttachmentsMacro().getDownloadAllLink().isPresent(), is(false));

        createTextAttachment(user1.get(), "test1.txt");
        assertThat(viewAttachmentsMacro().getDownloadAllLink().isPresent(), is(false));

        createTextAttachment(user1.get(), "test2.txt");
        assertThat(viewAttachmentsMacro().getDownloadAllLink().isPresent(), is(true));
    }

    @Test
    public void downloadLatestAttachmentVersion() throws Exception {
        final Content attachment1 = createTextAttachment(user1.get(), "test1.txt", "comment 1", "file 1 contents");
        final Content attachment2 = createTextAttachment(user1.get(), "test2.txt", "comment 2", "file 2 contents");

        final AttachmentsMacroComponent attachments = loginAndViewAttachmentsMacro();
        testUtils.fetchUrlAndCheckContent("file 1 contents", attachments.getAttachmentRow(attachment1).getAttachmentDownloadUrl(), user1.get());
        testUtils.fetchUrlAndCheckContent("file 2 contents", attachments.getAttachmentRow(attachment2).getAttachmentDownloadUrl(), user1.get());
    }

    @Test
    public void downloadAllAttachmentsZip() throws Exception {
        createTextAttachment(user1.get(), "test1.txt", "comment 1", "file 1 contents");
        createTextAttachment(user1.get(), "test2.txt", "comment 2", "file 2 contents");

        final URI downloadUrl = loginAndViewAttachmentsMacro().getDownloadAllURL();

        testUtils.downloadAndVerifyAttachmentsZip(downloadUrl, user1.get(), ImmutableMap.of(
                "test1.txt", "file 1 contents".getBytes(UTF_8),
                "test2.txt", "file 2 contents".getBytes(UTF_8)
        ));
    }

    private AttachmentsMacroComponent loginAndViewAttachmentsMacro() {
        return loginAndViewAttachmentsMacro(user1.get());
    }

    private AttachmentsMacroComponent loginAndViewAttachmentsMacro(UserWithDetails user) {
        final ViewPage viewPage = product.loginAndView(user, page);
        return getAttachmentsMacroComponent(viewPage);
    }

    private AttachmentsMacroComponent viewAttachmentsMacro() {
        final ViewPage viewPage = product.visit(ViewPage.class, page);
        return getAttachmentsMacroComponent(viewPage);
    }

    private AttachmentsMacroComponent getAttachmentsMacroComponent(ViewPage viewPage) {
        return viewPage.getComponent(AttachmentsMacroComponent.class, page);
    }

    private Content createTextAttachment(final UserWithDetails user) throws IOException {
        return createTextAttachment(user, "test.txt");
    }

    private Content createTextAttachment(final UserWithDetails user, String filename) throws IOException {
        return createAttachment(user, filename, "text/plain");
    }

    private Content createTextAttachment(final UserWithDetails user, String filename, String comment, String content) throws IOException {
        return createAttachment(user, filename, "text/plain", comment, content);
    }

    private Content createAttachment(final UserWithDetails user, String filename, String contentType) throws IOException {
        return createAttachment(user, filename, contentType, "Test file", "testing");
    }

    private Content createAttachment(final UserWithDetails user, String filename, String contentType, String comment, String content) throws IOException {
        final File tempFile = java.nio.file.Files.createTempFile(testName.getMethodName(), null).toFile();
        Files.write(content.getBytes(), tempFile);

        final PageResponse<Content> contents = attachmentService(user).addAttachments(
                page.getId(), Collections.singleton(
                        new AttachmentUpload(tempFile, filename, contentType, comment, false)
                )
        );

        return contents.getResults().iterator().next();
    }

    private static ContentService contentService(final UserWithDetails user) {
        return restClient.createSession(user).contentService();
    }

    private static AttachmentService attachmentService(final UserWithDetails user) {
        return restClient.createSession(user).attachmentService();
    }

    private static Pair<String, String> pair(@Nullable MacroParameter macroParameter) {
        return macroParameter == null
                ? null
                : Pair.pair(macroParameter.name(), macroParameter.value());
    }

    private static String buildAttachmentMacroDescriptor(@Nullable Pair<String, String> macroParameter) {
        String paramString = "";
        if (macroParameter != null) {
            paramString = String.format(":%s=%s", macroParameter.left(), macroParameter.right());
        }
        return String.format("{attachments%s}", paramString);
    }

}
